export const ssr = false; // Fetch client side, see https://kit.svelte.dev/docs/page-options#ssr

// Fetch common data from the backend
/** @type {import('./$types').PageLoad} */
export async function load({ fetch }) {
  const results = await fetch(
    `${import.meta.env.VITE_BACKEND_ADDRESS || 'http://localhost:8080'}/employees`,
  )
    .then((response) => {
      // Check if the request was successful (status code in the range 200-299)
      if (!response.ok) {
        throw new Error(`fetch error, status: ${response.status}`);
      }

      // Parse the response JSON or text
      return response.json(); // or response.text() for plain text
    })
    .catch((error) => {
      // Handle errors
      console.error('Fetch error:', error);
    });

  return results;
}

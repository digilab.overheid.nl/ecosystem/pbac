--
-- PostgreSQL database dump
--

-- Dumped from database version 16.3 (Debian 16.3-1.pgdg110+1)
-- Dumped by pg_dump version 16.3 (Debian 16.3-1.pgdg110+1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: contracts; Type: SCHEMA; Schema: -; Owner: -
--

CREATE SCHEMA contracts;


--
-- Name: peers; Type: SCHEMA; Schema: -; Owner: -
--

CREATE SCHEMA peers;


--
-- Name: content_hash_algorithm; Type: TYPE; Schema: contracts; Owner: -
--

CREATE TYPE contracts.content_hash_algorithm AS ENUM (
    'sha3_512'
);


--
-- Name: distribution_action; Type: TYPE; Schema: contracts; Owner: -
--

CREATE TYPE contracts.distribution_action AS ENUM (
    'submit_contract',
    'submit_accept_signature',
    'submit_reject_signature',
    'submit_revoke_signature'
);


--
-- Name: service_protocol_type; Type: TYPE; Schema: contracts; Owner: -
--

CREATE TYPE contracts.service_protocol_type AS ENUM (
    'PROTOCOL_TCP_HTTP_1.1',
    'PROTOCOL_TCP_HTTP_2'
);


--
-- Name: service_type; Type: TYPE; Schema: contracts; Owner: -
--

CREATE TYPE contracts.service_type AS ENUM (
    'service_type_service',
    'service_type_delegated_service'
);


--
-- Name: signature_type; Type: TYPE; Schema: contracts; Owner: -
--

CREATE TYPE contracts.signature_type AS ENUM (
    'accept',
    'reject',
    'revoke'
);


SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: content; Type: TABLE; Schema: contracts; Owner: -
--

CREATE TABLE contracts.content (
    hash character varying(150) NOT NULL,
    hash_algorithm contracts.content_hash_algorithm NOT NULL,
    iv uuid NOT NULL,
    group_id character varying(255) NOT NULL,
    valid_not_before timestamp with time zone NOT NULL,
    valid_not_after timestamp with time zone NOT NULL,
    created_at timestamp with time zone NOT NULL
);


--
-- Name: contract_signatures; Type: TABLE; Schema: contracts; Owner: -
--

CREATE TABLE contracts.contract_signatures (
    content_hash character varying(150) NOT NULL,
    signature_type contracts.signature_type NOT NULL,
    peer_id character varying(20) NOT NULL,
    certificate_thumbprint character varying(512) NOT NULL,
    signature character varying(2048) NOT NULL,
    signed_at timestamp with time zone NOT NULL
);


--
-- Name: failed_distributions; Type: TABLE; Schema: contracts; Owner: -
--

CREATE TABLE contracts.failed_distributions (
    peer_id character varying(20) NOT NULL,
    content_hash character varying(150) NOT NULL,
    signature character varying(2048) NOT NULL,
    action contracts.distribution_action NOT NULL,
    attempts integer DEFAULT 0 NOT NULL,
    last_attempt_at timestamp with time zone NOT NULL,
    next_attempt_at timestamp with time zone,
    reason character varying(1024) NOT NULL
);


--
-- Name: grants_delegated_service_connection; Type: TABLE; Schema: contracts; Owner: -
--

CREATE TABLE contracts.grants_delegated_service_connection (
    hash character varying(150) NOT NULL,
    content_hash character varying(150) NOT NULL,
    outway_peer_id character varying(20) NOT NULL,
    delegator_peer_id character varying(20) NOT NULL,
    service_peer_id character varying(20) NOT NULL,
    service_name character varying(255) NOT NULL,
    public_key_thumbprint character varying(64) NOT NULL,
    service_type contracts.service_type NOT NULL,
    service_publication_delegator_peer_id character varying(20),
    CONSTRAINT check_service_type CHECK ((((service_type = 'service_type_service'::contracts.service_type) AND (service_publication_delegator_peer_id IS NULL)) OR ((service_type = 'service_type_delegated_service'::contracts.service_type) AND (service_publication_delegator_peer_id IS NOT NULL))))
);


--
-- Name: grants_delegated_service_publication; Type: TABLE; Schema: contracts; Owner: -
--

CREATE TABLE contracts.grants_delegated_service_publication (
    hash character varying(150) NOT NULL,
    content_hash character varying(150) NOT NULL,
    directory_peer_id character varying(20) NOT NULL,
    delegator_peer_id character varying(20) NOT NULL,
    service_peer_id character varying(20) NOT NULL,
    service_name character varying(255) NOT NULL,
    service_protocol contracts.service_protocol_type NOT NULL
);


--
-- Name: grants_service_connection; Type: TABLE; Schema: contracts; Owner: -
--

CREATE TABLE contracts.grants_service_connection (
    hash character varying(150) NOT NULL,
    content_hash character varying(150) NOT NULL,
    consumer_peer_id character varying(20) NOT NULL,
    service_peer_id character varying(20) NOT NULL,
    service_name character varying(255) NOT NULL,
    public_key_thumbprint character varying(64) NOT NULL,
    service_type contracts.service_type NOT NULL,
    service_publication_delegator_peer_id character varying(20),
    CONSTRAINT check_service_type CHECK ((((service_type = 'service_type_service'::contracts.service_type) AND (service_publication_delegator_peer_id IS NULL)) OR ((service_type = 'service_type_delegated_service'::contracts.service_type) AND (service_publication_delegator_peer_id IS NOT NULL))))
);


--
-- Name: grants_service_publication; Type: TABLE; Schema: contracts; Owner: -
--

CREATE TABLE contracts.grants_service_publication (
    hash character varying(150) NOT NULL,
    content_hash character varying(150) NOT NULL,
    directory_peer_id character varying(20) NOT NULL,
    service_peer_id character varying(20) NOT NULL,
    service_name character varying(255) NOT NULL,
    service_protocol contracts.service_protocol_type NOT NULL
);


--
-- Name: valid_contracts; Type: VIEW; Schema: contracts; Owner: -
--

CREATE VIEW contracts.valid_contracts AS
 SELECT hash
   FROM contracts.content content
  WHERE ((valid_not_before < now()) AND (valid_not_after > now()) AND (( SELECT count(*) AS count
           FROM contracts.contract_signatures s
          WHERE ((s.signature_type = 'reject'::contracts.signature_type) AND ((s.content_hash)::text = (content.hash)::text))) = 0) AND (( SELECT count(*) AS count
           FROM contracts.contract_signatures s
          WHERE ((s.signature_type = 'revoke'::contracts.signature_type) AND ((s.content_hash)::text = (content.hash)::text))) = 0) AND (( SELECT count(DISTINCT p.id) AS count
           FROM ( SELECT grants_service_publication.directory_peer_id AS id
                   FROM contracts.grants_service_publication
                  WHERE ((grants_service_publication.content_hash)::text = (content.hash)::text)
                UNION
                 SELECT grants_service_publication.service_peer_id AS id
                   FROM contracts.grants_service_publication
                  WHERE ((grants_service_publication.content_hash)::text = (content.hash)::text)
                UNION
                 SELECT grants_service_connection.consumer_peer_id AS id
                   FROM contracts.grants_service_connection
                  WHERE ((grants_service_connection.content_hash)::text = (content.hash)::text)
                UNION
                 SELECT grants_service_connection.service_peer_id AS id
                   FROM contracts.grants_service_connection
                  WHERE ((grants_service_connection.content_hash)::text = (content.hash)::text)
                UNION
                 SELECT grants_service_connection.service_peer_id AS id
                   FROM contracts.grants_service_connection
                  WHERE ((grants_service_connection.content_hash)::text = (content.hash)::text)
                UNION
                 SELECT grants_service_connection.service_publication_delegator_peer_id AS id
                   FROM contracts.grants_service_connection
                  WHERE ((grants_service_connection.content_hash)::text = (content.hash)::text)
                UNION
                 SELECT grants_delegated_service_connection.service_peer_id AS id
                   FROM contracts.grants_delegated_service_connection
                  WHERE ((grants_delegated_service_connection.content_hash)::text = (content.hash)::text)
                UNION
                 SELECT grants_delegated_service_connection.outway_peer_id AS id
                   FROM contracts.grants_delegated_service_connection
                  WHERE ((grants_delegated_service_connection.content_hash)::text = (content.hash)::text)
                UNION
                 SELECT grants_delegated_service_connection.service_publication_delegator_peer_id AS id
                   FROM contracts.grants_delegated_service_connection
                  WHERE ((grants_delegated_service_connection.content_hash)::text = (content.hash)::text)
                UNION
                 SELECT grants_delegated_service_connection.delegator_peer_id AS id
                   FROM contracts.grants_delegated_service_connection
                  WHERE ((grants_delegated_service_connection.content_hash)::text = (content.hash)::text)
                UNION
                 SELECT grants_delegated_service_publication.service_peer_id AS id
                   FROM contracts.grants_delegated_service_publication
                  WHERE ((grants_delegated_service_publication.content_hash)::text = (content.hash)::text)
                UNION
                 SELECT grants_delegated_service_publication.directory_peer_id AS id
                   FROM contracts.grants_delegated_service_publication
                  WHERE ((grants_delegated_service_publication.content_hash)::text = (content.hash)::text)
                UNION
                 SELECT grants_delegated_service_publication.delegator_peer_id AS id
                   FROM contracts.grants_delegated_service_publication
                  WHERE ((grants_delegated_service_publication.content_hash)::text = (content.hash)::text)) p) = ( SELECT count(*) AS count
           FROM contracts.contract_signatures s
          WHERE ((s.signature_type = 'accept'::contracts.signature_type) AND ((s.content_hash)::text = (content.hash)::text)))));


--
-- Name: with_grant_type; Type: VIEW; Schema: contracts; Owner: -
--

CREATE VIEW contracts.with_grant_type AS
 SELECT DISTINCT ON (c.iv) c.iv,
    c.hash,
    c.hash_algorithm,
    c.group_id,
    c.valid_not_before,
    c.valid_not_after,
    c.created_at,
        CASE
            WHEN (gsc.consumer_peer_id IS NOT NULL) THEN true
            ELSE false
        END AS has_service_connection_grant,
        CASE
            WHEN (gdsc.outway_peer_id IS NOT NULL) THEN true
            ELSE false
        END AS has_delegated_service_connection_grant,
        CASE
            WHEN (gsp.service_name IS NOT NULL) THEN true
            ELSE false
        END AS has_service_publication_grant,
        CASE
            WHEN (gdsp.service_name IS NOT NULL) THEN true
            ELSE false
        END AS has_delegated_service_publication_grant
   FROM ((((contracts.content c
     LEFT JOIN contracts.grants_service_connection gsc ON (((gsc.content_hash)::text = (c.hash)::text)))
     LEFT JOIN contracts.grants_delegated_service_connection gdsc ON (((gdsc.content_hash)::text = (c.hash)::text)))
     LEFT JOIN contracts.grants_service_publication gsp ON (((gsp.content_hash)::text = (c.hash)::text)))
     LEFT JOIN contracts.grants_delegated_service_publication gdsp ON (((gdsp.content_hash)::text = (c.hash)::text)));


--
-- Name: certificates; Type: TABLE; Schema: peers; Owner: -
--

CREATE TABLE peers.certificates (
    certificate_thumbprint character varying(512) NOT NULL,
    peer_id character varying(20) NOT NULL,
    certificate bytea NOT NULL
);


--
-- Name: peers; Type: TABLE; Schema: peers; Owner: -
--

CREATE TABLE peers.peers (
    id character varying(20) NOT NULL,
    name character varying(60),
    manager_address character varying(255)
);


--
-- Name: schema_migrations; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.schema_migrations (
    version bigint NOT NULL,
    dirty boolean NOT NULL
);


--
-- Data for Name: content; Type: TABLE DATA; Schema: contracts; Owner: -
--

COPY contracts.content (hash, hash_algorithm, iv, group_id, valid_not_before, valid_not_after, created_at) FROM stdin;
\.


--
-- Data for Name: contract_signatures; Type: TABLE DATA; Schema: contracts; Owner: -
--

COPY contracts.contract_signatures (content_hash, signature_type, peer_id, certificate_thumbprint, signature, signed_at) FROM stdin;
\.


--
-- Data for Name: failed_distributions; Type: TABLE DATA; Schema: contracts; Owner: -
--

COPY contracts.failed_distributions (peer_id, content_hash, signature, action, attempts, last_attempt_at, next_attempt_at, reason) FROM stdin;
\.


--
-- Data for Name: grants_delegated_service_connection; Type: TABLE DATA; Schema: contracts; Owner: -
--

COPY contracts.grants_delegated_service_connection (hash, content_hash, outway_peer_id, delegator_peer_id, service_peer_id, service_name, public_key_thumbprint, service_type, service_publication_delegator_peer_id) FROM stdin;
\.


--
-- Data for Name: grants_delegated_service_publication; Type: TABLE DATA; Schema: contracts; Owner: -
--

COPY contracts.grants_delegated_service_publication (hash, content_hash, directory_peer_id, delegator_peer_id, service_peer_id, service_name, service_protocol) FROM stdin;
\.


--
-- Data for Name: grants_service_connection; Type: TABLE DATA; Schema: contracts; Owner: -
--

COPY contracts.grants_service_connection (hash, content_hash, consumer_peer_id, service_peer_id, service_name, public_key_thumbprint, service_type, service_publication_delegator_peer_id) FROM stdin;
\.


--
-- Data for Name: grants_service_publication; Type: TABLE DATA; Schema: contracts; Owner: -
--

COPY contracts.grants_service_publication (hash, content_hash, directory_peer_id, service_peer_id, service_name, service_protocol) FROM stdin;
\.


--
-- Data for Name: certificates; Type: TABLE DATA; Schema: peers; Owner: -
--

COPY peers.certificates (certificate_thumbprint, peer_id, certificate) FROM stdin;
55DTN32dDkb_0Y4Kzs19SJh5zZfdZpinbEHhoveRNCU	12345678901234567899	\\x308206463082042ea003020102021451cbcfe5897d36460d80e9c2b320bf36da038dd8300d06092a864886f70d01010d0500305f310b3009060355040613024e4c311630140603550408130d4e6f6f72642d486f6c6c616e643112301006035504071309416d7374657264616d31163014060355040a130d436f6d6d6f6e2047726f756e64310c300a060355040b13034e4c58301e170d3234303532323133323330305a170d3237303532323133323330305a30818d310b3009060355040613024e4c3110300e06035504081307557472656368743110300e0603550407130755747265636874310c300a060355040a1303666473312d302b060355040313246d616e616765722d6673632d6e6c782d6d616e616765722d65787465726e616c2e666473311d301b06035504051314313233343536373839303132333435363738393930820222300d06092a864886f70d01010105000382020f003082020a0282020100a7d1e061d7aa5c71ac8b4a39f96f76ea4ce2b1e2188601563bb9d33496eada2e52ec0d1683c1e6fcf63175379ce3f40ba3c2793c5932f5f6339f0ede52725a06546f828cafc07c5851d04f0d111f106e307dcd537b4b26311f30ca64b11d6ade31c983925c743e9209846d17b3f34c269a96f82648de48878b95cdcd4437adef8d9ebf35499747533bc623932bc21104a3fe07b14d5ed0090f569ac02156a2e5fa4d555c67637d97ade551d96ce2db002e689c8d2514c84b72c77ba21150228c05676b8135066d4010c087e2f02c4dc7b1a9ed1ba817dd6fe6eb6080fa4a26c9acb62411dce76f7f5b7c1a335d26a53f656979f57efe0f1d2df154820cfa04e6e22c5c467b3970c59795077461367481901c6b92ed2093009f775f8c9c254dfddba8041990d31e73ddd5d82aad965883ca7444d4f23639d7defa06671d39a64a5d1748b8d7f978f989a60fe55ef81232ecff0bfec709d23f60285bee6a1546c9a838a34da2228aac42f936af0150b2b0ea2e44db6a22322d5fb5da1ea9887129fab15dffc61ee51e00fae51f428f46cf41bb017e247d3b902f0330230b3372301c06fdb78bdcb8be9548849587245d6e35fa4e3c948e1d7a7b9be8fd5b32019435d73bd269eac9dba86383405edff1ad55a5c0deb7bb38281c7e0f5bb11557c92eb7d3c456ac54d26f237cd641cafb21c3887fe3b753a7d1a08fef27f57ca9770203010001a381ca3081c7300e0603551d0f0101ff0404030205a0301d0603551d250416301406082b0601050507030106082b06010505070302300c0603551d130101ff04023000301d0603551d0e04160414628a4575c61d9b480a8eff57a9f913e2aa6ea4c4301f0603551d230418301680145ae041511187acda6887c000c3065ce2792d89fd30480603551d110441303f82246d616e616765722d6673632d6e6c782d6d616e616765722d65787465726e616c2e6664738217696e7761792d6673632d6e6c782d696e7761792e666473300d06092a864886f70d01010d05000382020100842ae233f2a42fb8936be3801e991f468e507214a790e434ccd9cdd1fef29aa2e2f09c34a2889f8cf9c57d2322c6983c758f8dd17c4c78c0fa71466c9fd5011c70c4c0cf7cfd7ab0df79bf2ca0bd03f1384397cac7c240046f70b158007faf93180f430a2ec0651e2438c032f0f9a83b4398a802f095158c9057895b2dbcc23979b6fecf52b320d87e904a0ec631f7044ea1816e53afdcdc7250b706649a4c3a1c3d85c6f2df1025fa02b75d0e1f0b9f10a0c38735988621e608028744711dee70eb46a67e4bc5c6a7ffc4fe663b1ab8d353dcc8bece302d81b01a66ec58115422beb671874c2e112b956bb3f7c7fff819a2263c933ab28bb9520cdfc4e5d94f6d3ea21cd39512bcd8524af5d3c67122b0a738ee2aff31ccab691f1a62bfb232ddd7a81a914ca0f90ca0c9d3ce8b40f5952a29ab1505f3e68ae6bdc1f765f6bb92da938d180b9974d0c06842035c98fa7fee176fdad66c7bd1fe470bfa75f2fc2786fafee7f2621fa98abe46188df58d3d58dc0d2412305a873e041cd806638d5981e9a1a35ba9fcf546f620dfa0a9c46d29ab922c1c62650d28fe5ebd157b4ae50581e8d80b14457f234ae4631ac6a77e89e1b43838e9c4d4c8d6e42e72bc1fe57d07e3caae8c998e6a044d7ee3190f93165b41afc46a54209bfb8671415262d023c1354a0ee6a6023f936e09e8e28d18fc0e52a65808890061961fe05328d1
\.


--
-- Data for Name: peers; Type: TABLE DATA; Schema: peers; Owner: -
--

COPY peers.peers (id, name, manager_address) FROM stdin;
12345678901234567899	fds	https://manager-fsc-nlx-manager-external.fds:8443
01716384655448789995	orga	https://manager-fsc-nlx-manager-external.orga:8443
01716384713205186433	orgb	https://manager-fsc-nlx-manager-external.orgb:8443
\.


--
-- Data for Name: schema_migrations; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.schema_migrations (version, dirty) FROM stdin;
11	f
\.


--
-- Name: content content_pk; Type: CONSTRAINT; Schema: contracts; Owner: -
--

ALTER TABLE ONLY contracts.content
    ADD CONSTRAINT content_pk PRIMARY KEY (hash);


--
-- Name: contract_signatures contract_signatures_pk; Type: CONSTRAINT; Schema: contracts; Owner: -
--

ALTER TABLE ONLY contracts.contract_signatures
    ADD CONSTRAINT contract_signatures_pk PRIMARY KEY (content_hash, signature_type, peer_id);


--
-- Name: failed_distributions distribution_pk; Type: CONSTRAINT; Schema: contracts; Owner: -
--

ALTER TABLE ONLY contracts.failed_distributions
    ADD CONSTRAINT distribution_pk PRIMARY KEY (peer_id, content_hash, action);


--
-- Name: grants_delegated_service_connection grants_delegated_service_connection_pk; Type: CONSTRAINT; Schema: contracts; Owner: -
--

ALTER TABLE ONLY contracts.grants_delegated_service_connection
    ADD CONSTRAINT grants_delegated_service_connection_pk PRIMARY KEY (hash);


--
-- Name: grants_delegated_service_publication grants_delegated_service_publication_pk; Type: CONSTRAINT; Schema: contracts; Owner: -
--

ALTER TABLE ONLY contracts.grants_delegated_service_publication
    ADD CONSTRAINT grants_delegated_service_publication_pk PRIMARY KEY (hash);


--
-- Name: grants_service_connection grants_service_connection_pk; Type: CONSTRAINT; Schema: contracts; Owner: -
--

ALTER TABLE ONLY contracts.grants_service_connection
    ADD CONSTRAINT grants_service_connection_pk PRIMARY KEY (hash);


--
-- Name: grants_service_publication grants_service_publication_pk; Type: CONSTRAINT; Schema: contracts; Owner: -
--

ALTER TABLE ONLY contracts.grants_service_publication
    ADD CONSTRAINT grants_service_publication_pk PRIMARY KEY (hash);


--
-- Name: certificates certificates_pk; Type: CONSTRAINT; Schema: peers; Owner: -
--

ALTER TABLE ONLY peers.certificates
    ADD CONSTRAINT certificates_pk PRIMARY KEY (certificate_thumbprint);


--
-- Name: peers peers_pk; Type: CONSTRAINT; Schema: peers; Owner: -
--

ALTER TABLE ONLY peers.peers
    ADD CONSTRAINT peers_pk PRIMARY KEY (id);


--
-- Name: schema_migrations schema_migrations_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.schema_migrations
    ADD CONSTRAINT schema_migrations_pkey PRIMARY KEY (version);


--
-- Name: grants_delegated_service_connection_content_hash_idx; Type: INDEX; Schema: contracts; Owner: -
--

CREATE INDEX grants_delegated_service_connection_content_hash_idx ON contracts.grants_delegated_service_connection USING btree (content_hash);


--
-- Name: grants_delegated_service_connection_delegator_peer_id_idx; Type: INDEX; Schema: contracts; Owner: -
--

CREATE INDEX grants_delegated_service_connection_delegator_peer_id_idx ON contracts.grants_delegated_service_connection USING btree (delegator_peer_id);


--
-- Name: grants_delegated_service_connection_outway_peer_id_idx; Type: INDEX; Schema: contracts; Owner: -
--

CREATE INDEX grants_delegated_service_connection_outway_peer_id_idx ON contracts.grants_delegated_service_connection USING btree (outway_peer_id);


--
-- Name: grants_delegated_service_connection_service_peer_id_idx; Type: INDEX; Schema: contracts; Owner: -
--

CREATE INDEX grants_delegated_service_connection_service_peer_id_idx ON contracts.grants_delegated_service_connection USING btree (service_peer_id);


--
-- Name: grants_service_connection_consumer_peer_id_idx; Type: INDEX; Schema: contracts; Owner: -
--

CREATE INDEX grants_service_connection_consumer_peer_id_idx ON contracts.grants_service_connection USING btree (consumer_peer_id);


--
-- Name: grants_service_connection_content_hash_idx; Type: INDEX; Schema: contracts; Owner: -
--

CREATE INDEX grants_service_connection_content_hash_idx ON contracts.grants_service_connection USING btree (content_hash);


--
-- Name: grants_service_connection_service_peer_id_idx; Type: INDEX; Schema: contracts; Owner: -
--

CREATE INDEX grants_service_connection_service_peer_id_idx ON contracts.grants_service_connection USING btree (service_peer_id);


--
-- Name: grants_service_publication_directory_peer_id_idx; Type: INDEX; Schema: contracts; Owner: -
--

CREATE INDEX grants_service_publication_directory_peer_id_idx ON contracts.grants_service_publication USING btree (directory_peer_id);


--
-- Name: grants_service_publication_service_peer_id_idx; Type: INDEX; Schema: contracts; Owner: -
--

CREATE INDEX grants_service_publication_service_peer_id_idx ON contracts.grants_service_publication USING btree (service_peer_id);


--
-- Name: certificates_certificate_thumbprint_idx; Type: INDEX; Schema: peers; Owner: -
--

CREATE INDEX certificates_certificate_thumbprint_idx ON peers.certificates USING btree (certificate_thumbprint);


--
-- Name: certificates_peer_id_idx; Type: INDEX; Schema: peers; Owner: -
--

CREATE INDEX certificates_peer_id_idx ON peers.certificates USING btree (peer_id);


--
-- Name: contract_signatures contract_signatures_content_hash_fk; Type: FK CONSTRAINT; Schema: contracts; Owner: -
--

ALTER TABLE ONLY contracts.contract_signatures
    ADD CONSTRAINT contract_signatures_content_hash_fk FOREIGN KEY (content_hash) REFERENCES contracts.content(hash) MATCH FULL ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: contract_signatures contract_signatures_peer_id_fk; Type: FK CONSTRAINT; Schema: contracts; Owner: -
--

ALTER TABLE ONLY contracts.contract_signatures
    ADD CONSTRAINT contract_signatures_peer_id_fk FOREIGN KEY (peer_id) REFERENCES peers.peers(id) MATCH FULL ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: failed_distributions distribution_content_hash; Type: FK CONSTRAINT; Schema: contracts; Owner: -
--

ALTER TABLE ONLY contracts.failed_distributions
    ADD CONSTRAINT distribution_content_hash FOREIGN KEY (content_hash) REFERENCES contracts.content(hash) MATCH FULL ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: failed_distributions distribution_peer_id; Type: FK CONSTRAINT; Schema: contracts; Owner: -
--

ALTER TABLE ONLY contracts.failed_distributions
    ADD CONSTRAINT distribution_peer_id FOREIGN KEY (peer_id) REFERENCES peers.peers(id) MATCH FULL ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: grants_delegated_service_connection grants_delegated_service_connection_content_hash_fk; Type: FK CONSTRAINT; Schema: contracts; Owner: -
--

ALTER TABLE ONLY contracts.grants_delegated_service_connection
    ADD CONSTRAINT grants_delegated_service_connection_content_hash_fk FOREIGN KEY (content_hash) REFERENCES contracts.content(hash) MATCH FULL ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: grants_delegated_service_connection grants_delegated_service_connection_delegator_peer_id_fk; Type: FK CONSTRAINT; Schema: contracts; Owner: -
--

ALTER TABLE ONLY contracts.grants_delegated_service_connection
    ADD CONSTRAINT grants_delegated_service_connection_delegator_peer_id_fk FOREIGN KEY (delegator_peer_id) REFERENCES peers.peers(id) MATCH FULL ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: grants_delegated_service_connection grants_delegated_service_connection_outway_peer_id_fk; Type: FK CONSTRAINT; Schema: contracts; Owner: -
--

ALTER TABLE ONLY contracts.grants_delegated_service_connection
    ADD CONSTRAINT grants_delegated_service_connection_outway_peer_id_fk FOREIGN KEY (outway_peer_id) REFERENCES peers.peers(id) MATCH FULL ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: grants_delegated_service_connection grants_delegated_service_connection_service_peer_id_fk; Type: FK CONSTRAINT; Schema: contracts; Owner: -
--

ALTER TABLE ONLY contracts.grants_delegated_service_connection
    ADD CONSTRAINT grants_delegated_service_connection_service_peer_id_fk FOREIGN KEY (service_peer_id) REFERENCES peers.peers(id) MATCH FULL ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: grants_service_connection grants_service_connection_content_hash_fk; Type: FK CONSTRAINT; Schema: contracts; Owner: -
--

ALTER TABLE ONLY contracts.grants_service_connection
    ADD CONSTRAINT grants_service_connection_content_hash_fk FOREIGN KEY (content_hash) REFERENCES contracts.content(hash) MATCH FULL ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: grants_service_connection grants_service_connections_consumer_peer_id_fk; Type: FK CONSTRAINT; Schema: contracts; Owner: -
--

ALTER TABLE ONLY contracts.grants_service_connection
    ADD CONSTRAINT grants_service_connections_consumer_peer_id_fk FOREIGN KEY (consumer_peer_id) REFERENCES peers.peers(id) MATCH FULL ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: grants_service_connection grants_service_connections_service_peer_id_fk; Type: FK CONSTRAINT; Schema: contracts; Owner: -
--

ALTER TABLE ONLY contracts.grants_service_connection
    ADD CONSTRAINT grants_service_connections_service_peer_id_fk FOREIGN KEY (service_peer_id) REFERENCES peers.peers(id) MATCH FULL ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: grants_service_publication grants_service_publication_directory_peer_id_fk; Type: FK CONSTRAINT; Schema: contracts; Owner: -
--

ALTER TABLE ONLY contracts.grants_service_publication
    ADD CONSTRAINT grants_service_publication_directory_peer_id_fk FOREIGN KEY (directory_peer_id) REFERENCES peers.peers(id) MATCH FULL ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: grants_service_publication grants_service_publication_service_peer_id_fk; Type: FK CONSTRAINT; Schema: contracts; Owner: -
--

ALTER TABLE ONLY contracts.grants_service_publication
    ADD CONSTRAINT grants_service_publication_service_peer_id_fk FOREIGN KEY (service_peer_id) REFERENCES peers.peers(id) MATCH FULL ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: certificates certificates_peer_id_fk; Type: FK CONSTRAINT; Schema: peers; Owner: -
--

ALTER TABLE ONLY peers.certificates
    ADD CONSTRAINT certificates_peer_id_fk FOREIGN KEY (peer_id) REFERENCES peers.peers(id) MATCH FULL ON DELETE CASCADE;


--
-- PostgreSQL database dump complete
--


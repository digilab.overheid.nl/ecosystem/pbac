--
-- PostgreSQL database dump
--

-- Dumped from database version 16.3 (Debian 16.3-1.pgdg110+1)
-- Dumped by pg_dump version 16.3 (Debian 16.3-1.pgdg110+1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: contracts; Type: SCHEMA; Schema: -; Owner: -
--

CREATE SCHEMA contracts;


--
-- Name: peers; Type: SCHEMA; Schema: -; Owner: -
--

CREATE SCHEMA peers;


--
-- Name: content_hash_algorithm; Type: TYPE; Schema: contracts; Owner: -
--

CREATE TYPE contracts.content_hash_algorithm AS ENUM (
    'sha3_512'
);


--
-- Name: distribution_action; Type: TYPE; Schema: contracts; Owner: -
--

CREATE TYPE contracts.distribution_action AS ENUM (
    'submit_contract',
    'submit_accept_signature',
    'submit_reject_signature',
    'submit_revoke_signature'
);


--
-- Name: service_protocol_type; Type: TYPE; Schema: contracts; Owner: -
--

CREATE TYPE contracts.service_protocol_type AS ENUM (
    'PROTOCOL_TCP_HTTP_1.1',
    'PROTOCOL_TCP_HTTP_2'
);


--
-- Name: service_type; Type: TYPE; Schema: contracts; Owner: -
--

CREATE TYPE contracts.service_type AS ENUM (
    'service_type_service',
    'service_type_delegated_service'
);


--
-- Name: signature_type; Type: TYPE; Schema: contracts; Owner: -
--

CREATE TYPE contracts.signature_type AS ENUM (
    'accept',
    'reject',
    'revoke'
);


SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: content; Type: TABLE; Schema: contracts; Owner: -
--

CREATE TABLE contracts.content (
    hash character varying(150) NOT NULL,
    hash_algorithm contracts.content_hash_algorithm NOT NULL,
    iv uuid NOT NULL,
    group_id character varying(255) NOT NULL,
    valid_not_before timestamp with time zone NOT NULL,
    valid_not_after timestamp with time zone NOT NULL,
    created_at timestamp with time zone NOT NULL
);


--
-- Name: contract_signatures; Type: TABLE; Schema: contracts; Owner: -
--

CREATE TABLE contracts.contract_signatures (
    content_hash character varying(150) NOT NULL,
    signature_type contracts.signature_type NOT NULL,
    peer_id character varying(20) NOT NULL,
    certificate_thumbprint character varying(512) NOT NULL,
    signature character varying(2048) NOT NULL,
    signed_at timestamp with time zone NOT NULL
);


--
-- Name: failed_distributions; Type: TABLE; Schema: contracts; Owner: -
--

CREATE TABLE contracts.failed_distributions (
    peer_id character varying(20) NOT NULL,
    content_hash character varying(150) NOT NULL,
    signature character varying(2048) NOT NULL,
    action contracts.distribution_action NOT NULL,
    attempts integer DEFAULT 0 NOT NULL,
    last_attempt_at timestamp with time zone NOT NULL,
    next_attempt_at timestamp with time zone,
    reason character varying(1024) NOT NULL
);


--
-- Name: grants_delegated_service_connection; Type: TABLE; Schema: contracts; Owner: -
--

CREATE TABLE contracts.grants_delegated_service_connection (
    hash character varying(150) NOT NULL,
    content_hash character varying(150) NOT NULL,
    outway_peer_id character varying(20) NOT NULL,
    delegator_peer_id character varying(20) NOT NULL,
    service_peer_id character varying(20) NOT NULL,
    service_name character varying(255) NOT NULL,
    public_key_thumbprint character varying(64) NOT NULL,
    service_type contracts.service_type NOT NULL,
    service_publication_delegator_peer_id character varying(20),
    CONSTRAINT check_service_type CHECK ((((service_type = 'service_type_service'::contracts.service_type) AND (service_publication_delegator_peer_id IS NULL)) OR ((service_type = 'service_type_delegated_service'::contracts.service_type) AND (service_publication_delegator_peer_id IS NOT NULL))))
);


--
-- Name: grants_delegated_service_publication; Type: TABLE; Schema: contracts; Owner: -
--

CREATE TABLE contracts.grants_delegated_service_publication (
    hash character varying(150) NOT NULL,
    content_hash character varying(150) NOT NULL,
    directory_peer_id character varying(20) NOT NULL,
    delegator_peer_id character varying(20) NOT NULL,
    service_peer_id character varying(20) NOT NULL,
    service_name character varying(255) NOT NULL,
    service_protocol contracts.service_protocol_type NOT NULL
);


--
-- Name: grants_service_connection; Type: TABLE; Schema: contracts; Owner: -
--

CREATE TABLE contracts.grants_service_connection (
    hash character varying(150) NOT NULL,
    content_hash character varying(150) NOT NULL,
    consumer_peer_id character varying(20) NOT NULL,
    service_peer_id character varying(20) NOT NULL,
    service_name character varying(255) NOT NULL,
    public_key_thumbprint character varying(64) NOT NULL,
    service_type contracts.service_type NOT NULL,
    service_publication_delegator_peer_id character varying(20),
    CONSTRAINT check_service_type CHECK ((((service_type = 'service_type_service'::contracts.service_type) AND (service_publication_delegator_peer_id IS NULL)) OR ((service_type = 'service_type_delegated_service'::contracts.service_type) AND (service_publication_delegator_peer_id IS NOT NULL))))
);


--
-- Name: grants_service_publication; Type: TABLE; Schema: contracts; Owner: -
--

CREATE TABLE contracts.grants_service_publication (
    hash character varying(150) NOT NULL,
    content_hash character varying(150) NOT NULL,
    directory_peer_id character varying(20) NOT NULL,
    service_peer_id character varying(20) NOT NULL,
    service_name character varying(255) NOT NULL,
    service_protocol contracts.service_protocol_type NOT NULL
);


--
-- Name: valid_contracts; Type: VIEW; Schema: contracts; Owner: -
--

CREATE VIEW contracts.valid_contracts AS
 SELECT hash
   FROM contracts.content content
  WHERE ((valid_not_before < now()) AND (valid_not_after > now()) AND (( SELECT count(*) AS count
           FROM contracts.contract_signatures s
          WHERE ((s.signature_type = 'reject'::contracts.signature_type) AND ((s.content_hash)::text = (content.hash)::text))) = 0) AND (( SELECT count(*) AS count
           FROM contracts.contract_signatures s
          WHERE ((s.signature_type = 'revoke'::contracts.signature_type) AND ((s.content_hash)::text = (content.hash)::text))) = 0) AND (( SELECT count(DISTINCT p.id) AS count
           FROM ( SELECT grants_service_publication.directory_peer_id AS id
                   FROM contracts.grants_service_publication
                  WHERE ((grants_service_publication.content_hash)::text = (content.hash)::text)
                UNION
                 SELECT grants_service_publication.service_peer_id AS id
                   FROM contracts.grants_service_publication
                  WHERE ((grants_service_publication.content_hash)::text = (content.hash)::text)
                UNION
                 SELECT grants_service_connection.consumer_peer_id AS id
                   FROM contracts.grants_service_connection
                  WHERE ((grants_service_connection.content_hash)::text = (content.hash)::text)
                UNION
                 SELECT grants_service_connection.service_peer_id AS id
                   FROM contracts.grants_service_connection
                  WHERE ((grants_service_connection.content_hash)::text = (content.hash)::text)
                UNION
                 SELECT grants_service_connection.service_peer_id AS id
                   FROM contracts.grants_service_connection
                  WHERE ((grants_service_connection.content_hash)::text = (content.hash)::text)
                UNION
                 SELECT grants_service_connection.service_publication_delegator_peer_id AS id
                   FROM contracts.grants_service_connection
                  WHERE ((grants_service_connection.content_hash)::text = (content.hash)::text)
                UNION
                 SELECT grants_delegated_service_connection.service_peer_id AS id
                   FROM contracts.grants_delegated_service_connection
                  WHERE ((grants_delegated_service_connection.content_hash)::text = (content.hash)::text)
                UNION
                 SELECT grants_delegated_service_connection.outway_peer_id AS id
                   FROM contracts.grants_delegated_service_connection
                  WHERE ((grants_delegated_service_connection.content_hash)::text = (content.hash)::text)
                UNION
                 SELECT grants_delegated_service_connection.service_publication_delegator_peer_id AS id
                   FROM contracts.grants_delegated_service_connection
                  WHERE ((grants_delegated_service_connection.content_hash)::text = (content.hash)::text)
                UNION
                 SELECT grants_delegated_service_connection.delegator_peer_id AS id
                   FROM contracts.grants_delegated_service_connection
                  WHERE ((grants_delegated_service_connection.content_hash)::text = (content.hash)::text)
                UNION
                 SELECT grants_delegated_service_publication.service_peer_id AS id
                   FROM contracts.grants_delegated_service_publication
                  WHERE ((grants_delegated_service_publication.content_hash)::text = (content.hash)::text)
                UNION
                 SELECT grants_delegated_service_publication.directory_peer_id AS id
                   FROM contracts.grants_delegated_service_publication
                  WHERE ((grants_delegated_service_publication.content_hash)::text = (content.hash)::text)
                UNION
                 SELECT grants_delegated_service_publication.delegator_peer_id AS id
                   FROM contracts.grants_delegated_service_publication
                  WHERE ((grants_delegated_service_publication.content_hash)::text = (content.hash)::text)) p) = ( SELECT count(*) AS count
           FROM contracts.contract_signatures s
          WHERE ((s.signature_type = 'accept'::contracts.signature_type) AND ((s.content_hash)::text = (content.hash)::text)))));


--
-- Name: with_grant_type; Type: VIEW; Schema: contracts; Owner: -
--

CREATE VIEW contracts.with_grant_type AS
 SELECT DISTINCT ON (c.iv) c.iv,
    c.hash,
    c.hash_algorithm,
    c.group_id,
    c.valid_not_before,
    c.valid_not_after,
    c.created_at,
        CASE
            WHEN (gsc.consumer_peer_id IS NOT NULL) THEN true
            ELSE false
        END AS has_service_connection_grant,
        CASE
            WHEN (gdsc.outway_peer_id IS NOT NULL) THEN true
            ELSE false
        END AS has_delegated_service_connection_grant,
        CASE
            WHEN (gsp.service_name IS NOT NULL) THEN true
            ELSE false
        END AS has_service_publication_grant,
        CASE
            WHEN (gdsp.service_name IS NOT NULL) THEN true
            ELSE false
        END AS has_delegated_service_publication_grant
   FROM ((((contracts.content c
     LEFT JOIN contracts.grants_service_connection gsc ON (((gsc.content_hash)::text = (c.hash)::text)))
     LEFT JOIN contracts.grants_delegated_service_connection gdsc ON (((gdsc.content_hash)::text = (c.hash)::text)))
     LEFT JOIN contracts.grants_service_publication gsp ON (((gsp.content_hash)::text = (c.hash)::text)))
     LEFT JOIN contracts.grants_delegated_service_publication gdsp ON (((gdsp.content_hash)::text = (c.hash)::text)));


--
-- Name: certificates; Type: TABLE; Schema: peers; Owner: -
--

CREATE TABLE peers.certificates (
    certificate_thumbprint character varying(512) NOT NULL,
    peer_id character varying(20) NOT NULL,
    certificate bytea NOT NULL
);


--
-- Name: peers; Type: TABLE; Schema: peers; Owner: -
--

CREATE TABLE peers.peers (
    id character varying(20) NOT NULL,
    name character varying(60),
    manager_address character varying(255)
);


--
-- Name: schema_migrations; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.schema_migrations (
    version bigint NOT NULL,
    dirty boolean NOT NULL
);


--
-- Data for Name: content; Type: TABLE DATA; Schema: contracts; Owner: -
--

COPY contracts.content (hash, hash_algorithm, iv, group_id, valid_not_before, valid_not_after, created_at) FROM stdin;
$1$1$Ewn61gOUJBLgyGl020S5sXjzO2rNHvpEOVihjVsRg3w3aoawJiIKLP6OI4U-hhout-3nmbHWyICY1aCGO2UbLg	sha3_512	0190a69f-e001-7ba6-82fc-c1362390980b	fsc-demo	2024-07-12 00:00:00+00	2053-07-13 00:00:00+00	2024-07-12 11:06:53+00
$1$1$t7Xy1i2N22ofk3mEX9mFiIMvJpif0leaw3EjEkTg9agoDnlaaYKJRUTdb8nyCAOTAkvkUhbK7q1yarXPVbt79Q	sha3_512	0190baeb-917f-74ab-98e1-826912dd9eea	fsc-demo	2024-07-16 00:00:00+00	2028-07-17 00:00:00+00	2024-07-16 09:41:58+00
\.


--
-- Data for Name: contract_signatures; Type: TABLE DATA; Schema: contracts; Owner: -
--

COPY contracts.contract_signatures (content_hash, signature_type, peer_id, certificate_thumbprint, signature, signed_at) FROM stdin;
$1$1$Ewn61gOUJBLgyGl020S5sXjzO2rNHvpEOVihjVsRg3w3aoawJiIKLP6OI4U-hhout-3nmbHWyICY1aCGO2UbLg	accept	01716384655448789995	7FjZxQyckHjVkuNYQ9lrYP6qoGRi-0a_JUjjv-6HetY	eyJhbGciOiJSUzUxMiIsIng1dCNTMjU2IjoiN0ZqWnhReWNrSGpWa3VOWVE5bHJZUDZxb0dSaS0wYV9KVWpqdi02SGV0WSJ9.eyJjb250ZW50SGFzaCI6IiQxJDEkRXduNjFnT1VKQkxneUdsMDIwUzVzWGp6TzJyTkh2cEVPVmloalZzUmczdzNhb2F3SmlJS0xQNk9JNFUtaGhvdXQtM25tYkhXeUlDWTFhQ0dPMlViTGciLCJ0eXBlIjoiYWNjZXB0Iiwic2lnbmVkQXQiOjE3MjA3ODI0MTN9.P0rx25qcfsaYeUJWCAjZID1D_ivrnADZRkIlzAb1M4nDJhfjkgSGEjtlRGOqrB_ZQMdadNyWQJRDWXgriah5ipksMJVwzxiIzndJAFXHacJkgKF5NuCCxJ8XT7X-_wbvaJqky_pO1DIh2Ild0UDhTTwF57GiLcRg8y8tFHD4MVQC7EIp5IDuRG-GjnXHDuglSFUWN4KHs928yFHIQXQPBs99DAsryJBbaB926DfcGp2eHWqDgQe-TTYPGJghxguDvcnN61IqQjhAUTWws4so5Pak3_urfhuavIndj5mJKop0dqD6DKkoVh0MsrFnR1LTgR5kTzZ94IdpRj_4rqD4BrQnyEZAI9qW6OSmTA9gccxPbjtjajsrwlMW8z2pQSwNHAJF_QEMmm4pOabNH_A1htWZTtwH_9vpNcp2zR-CIdf_AhV_7hnThvZoCnRiX6_i74BUkU9Uu3JRS2InVNZOIRPZkphlKRAV-PWzM_MGRVbWwgiouIMcjzAR_zDltLRxal0N7FanLJkoyW5993PQiv-vxXNmGGqxyRStlqHgvWMxzB9uNP69PPRa7ty26K5Qer8l3LWp7jFxcr1shpG1KzZTE_QfhUIpdyICM1XFNruUkf7Sylk0CwutjNdPdXDihHDp0oHEI2ZWm9Np8_8M1kxoR8UQKxxqAPFNtcARxEY	2024-07-12 11:06:53+00
$1$1$Ewn61gOUJBLgyGl020S5sXjzO2rNHvpEOVihjVsRg3w3aoawJiIKLP6OI4U-hhout-3nmbHWyICY1aCGO2UbLg	accept	01716384713205186433	M-lXHT1DRfNebmBxrr9HYYIAx02sn9hynnJBkIGsxn4	eyJhbGciOiJSUzUxMiIsIng1dCNTMjU2IjoiTS1sWEhUMURSZk5lYm1CeHJyOUhZWUlBeDAyc245aHlubkpCa0lHc3huNCJ9.eyJjb250ZW50SGFzaCI6IiQxJDEkRXduNjFnT1VKQkxneUdsMDIwUzVzWGp6TzJyTkh2cEVPVmloalZzUmczdzNhb2F3SmlJS0xQNk9JNFUtaGhvdXQtM25tYkhXeUlDWTFhQ0dPMlViTGciLCJ0eXBlIjoiYWNjZXB0Iiwic2lnbmVkQXQiOjE3MjA3ODI0MjF9.hbdupok-HBmIa2ZD_6tmJ_CRRBusXeZuOU5zSbAwFgdnupQq4tIIJdj3v03r2zt479ZdHzuSNFuGKVMcPRj5afvLBX0fVI0skzbBbZyK3Rtov20p9YZivsgJDEnDhyksvgSSsLZJ5Zbchta95NhHcxpChHhJHUR46FFameonUA8fThShjWhVXcPOMdPYSbNXUIFhz3DNukLPCN6WRpSZGco8zjZr-VH75Vk8lk6MSdrWEL8C49EcMOx-_RvEyPjkCu_gATI4jN9pBjk7ZIgzLu-tMO84rQHNj9j4R7Mw3JPBPRvaHc2d6Zcbp22HRJ1Xrsk8ZH1_3_TuHNRKrwGdYrcJQw6A_DJFuOikfBaIotciMp0IbDDXtXp4IHPKR_RHbkR9LSE3r_mQWr6t_DUKuts-O9nOh9E1VlCYqRrDrCGGhcTl8SBHd1pcgWP-RuikaZxZb1cAhlukczJJSuyFM-0SWi4rVe5CCcj-6ALXrwfbnWIqTyhCzlk9LclZPz-eUO7TqeJE15C1xDTbYcT7-Jxf-gUTbtA7ETpa4_d_VTkL1dByx3iAmH_CweEo1fdemftihmCXDi_9uvNqCVF2b4VzXE_PAOHaV0lv3XdHkx50w5UO-hbvIrmOdV0i0Az7HcG33Gnpq7NvvcbUdEH3B0fv0ObeIFIUIkX7-Wys_zE	2024-07-12 11:07:01+00
$1$1$t7Xy1i2N22ofk3mEX9mFiIMvJpif0leaw3EjEkTg9agoDnlaaYKJRUTdb8nyCAOTAkvkUhbK7q1yarXPVbt79Q	accept	01716384655448789995	7FjZxQyckHjVkuNYQ9lrYP6qoGRi-0a_JUjjv-6HetY	eyJhbGciOiJSUzUxMiIsIng1dCNTMjU2IjoiN0ZqWnhReWNrSGpWa3VOWVE5bHJZUDZxb0dSaS0wYV9KVWpqdi02SGV0WSJ9.eyJjb250ZW50SGFzaCI6IiQxJDEkdDdYeTFpMk4yMm9mazNtRVg5bUZpSU12SnBpZjBsZWF3M0VqRWtUZzlhZ29EbmxhYVlLSlJVVGRiOG55Q0FPVEFrdmtVaGJLN3ExeWFyWFBWYnQ3OVEiLCJ0eXBlIjoiYWNjZXB0Iiwic2lnbmVkQXQiOjE3MjExMjI5MTh9.DJCTITDzL2NqXOJV2d3Y_kXj9-wAyP69aR1UoePyX-AuhtfVWMamUdji-EMQekiBwwhmazEpqtK1u-QIqLRzj0fYtLOf-R9dy-8IyxJMA7ahsl_orzRQGBIkoTDV4migVgbCbcPYSxm0M0_uZite4_hMi199SaFoqFaiwmbVrnrEgILdAtPxyHGq1QHs-dzyJWs-JBpXZgGuyf5eT_pByxb2qHkm-oaApeGjYpqfa7lp9-h1p7iQRXFLsduTlzbByST1EiUQmiSuic18F3QwHvXfnfmLiRqCpa9_eLtZbI0887rlzgBEXgKh0fmuRD34RJFsTea-tKGajfLOXnXZ4ZnsHr3_vPPHxmTcVUWrgYDWNeCnWTP_RiPKG3NOi02-jxC14LSMglv3vfZjDCeoq5pOX831iTVC-MVJ7hAtsm-ltqGRSjFbzOtAn8QWjckinpZqC7CO5qiNkIOD-qnO8CW8_KhlE0noF0P4pWuUT-3f0rh-pZTgH-VXrs1a0CzZvdP9ZgUjm6-mwWHMKzwI0TAAE6ws8akNgH4lqqFKMO9qQeYW4K_x9FIKN9FG_n82Rlbcyb3CGr2n7akT-wT56Ski7Z9fut4RgvpVaoxL6hcf-rTvzyhgZ6sST79u0ugsGcNKaN_YZswwECOLj5fJCkQ8JLSjRGzQN1aI03so5Sc	2024-07-16 09:41:58+00
$1$1$t7Xy1i2N22ofk3mEX9mFiIMvJpif0leaw3EjEkTg9agoDnlaaYKJRUTdb8nyCAOTAkvkUhbK7q1yarXPVbt79Q	accept	01716384713205186433	M-lXHT1DRfNebmBxrr9HYYIAx02sn9hynnJBkIGsxn4	eyJhbGciOiJSUzUxMiIsIng1dCNTMjU2IjoiTS1sWEhUMURSZk5lYm1CeHJyOUhZWUlBeDAyc245aHlubkpCa0lHc3huNCJ9.eyJjb250ZW50SGFzaCI6IiQxJDEkdDdYeTFpMk4yMm9mazNtRVg5bUZpSU12SnBpZjBsZWF3M0VqRWtUZzlhZ29EbmxhYVlLSlJVVGRiOG55Q0FPVEFrdmtVaGJLN3ExeWFyWFBWYnQ3OVEiLCJ0eXBlIjoiYWNjZXB0Iiwic2lnbmVkQXQiOjE3MjExMjI5MjV9.6xp3sSe3O2YBLuJzYbNBP5ujCKh6v1WdunIl4YkMgPuD95M4dedyiiqdOI0KSGQ_QkzNVmlVImrnp6soRhR1kAFdoe4cU73x_6zckJMHuBvs7aPmDAOqxl3ytj-3ta0aesV9h7IvuatSD4itQIBhDFv7BnA53RfzAOngmyZrOD7QWus1GpV_rqMRvZnFa283JEyrJiIGPGrscf551xBwnzkbc51NKiObjGU9Is_3cpTSoroLO3TFbyJVFH7y89_Uyew5TtWTPtWV6sxQ8ZFWbe-Yq5IqQ5U1lseZ89CjQ3JlVbrvallO8F6krU_W3n7av1PIUWwt_2j8GD1iwhgRqs8R5bhRWKm453-oyElgz8InfgVcnpXTGuJSvU5faHpW8O570Re3lWpR9A-2xrwy99i3YS7ewUls_HSte7eWfkl8O8uJK6awCkd8oX5G_cMREDvRe0rv0cKy4JCdwKSO6B4LlW-hIj6xlFVQ-LEJXYW_HZ-saIQVKkhimdQiSR_Uag-RqNFL77b4sgNz6DLNb7T0ini3Lo7EUNU6nFKSrxVX8CmF-ONo75VP3hgR9ytfhe5U4Fm3qF4uZAVoIzAmVPxD6_rMdabRCUZfT8ky2VF_7Vv1HLmnA2TdU7sPYkAA1DkelOv63FzTNBM9B2ppG480WAJL-8urGflnPAUnVVo	2024-07-16 09:42:05+00
\.


--
-- Data for Name: failed_distributions; Type: TABLE DATA; Schema: contracts; Owner: -
--

COPY contracts.failed_distributions (peer_id, content_hash, signature, action, attempts, last_attempt_at, next_attempt_at, reason) FROM stdin;
\.


--
-- Data for Name: grants_delegated_service_connection; Type: TABLE DATA; Schema: contracts; Owner: -
--

COPY contracts.grants_delegated_service_connection (hash, content_hash, outway_peer_id, delegator_peer_id, service_peer_id, service_name, public_key_thumbprint, service_type, service_publication_delegator_peer_id) FROM stdin;
\.


--
-- Data for Name: grants_delegated_service_publication; Type: TABLE DATA; Schema: contracts; Owner: -
--

COPY contracts.grants_delegated_service_publication (hash, content_hash, directory_peer_id, delegator_peer_id, service_peer_id, service_name, service_protocol) FROM stdin;
\.


--
-- Data for Name: grants_service_connection; Type: TABLE DATA; Schema: contracts; Owner: -
--

COPY contracts.grants_service_connection (hash, content_hash, consumer_peer_id, service_peer_id, service_name, public_key_thumbprint, service_type, service_publication_delegator_peer_id) FROM stdin;
$1$3$vaoBMJsm1fRHNd959nxkCK6pq-FWYm04kI2IejnVpuvkItl0FWFO_4itxNnnPQvotHq4qVQ1YDNPrLnerQFnEQ	$1$1$Ewn61gOUJBLgyGl020S5sXjzO2rNHvpEOVihjVsRg3w3aoawJiIKLP6OI4U-hhout-3nmbHWyICY1aCGO2UbLg	01716384655448789995	01716384713205186433	echo	3a33dc35d52b7bb777889c868b1ec4865aefa0dc16290bf251ccadd54e48f847	service_type_delegated_service	01716384655448789995
$1$3$e68YEU2shhvhavA3ERLAPRqf5EsZopxO0zYR_A34xZyK1iuNzLzYBSVWn3U0sHjuGpqkpL9XC7RsX36En8SqrA	$1$1$t7Xy1i2N22ofk3mEX9mFiIMvJpif0leaw3EjEkTg9agoDnlaaYKJRUTdb8nyCAOTAkvkUhbK7q1yarXPVbt79Q	01716384655448789995	01716384713205186433	echo-logger	3a33dc35d52b7bb777889c868b1ec4865aefa0dc16290bf251ccadd54e48f847	service_type_delegated_service	01716384655448789995
\.


--
-- Data for Name: grants_service_publication; Type: TABLE DATA; Schema: contracts; Owner: -
--

COPY contracts.grants_service_publication (hash, content_hash, directory_peer_id, service_peer_id, service_name, service_protocol) FROM stdin;
\.


--
-- Data for Name: certificates; Type: TABLE DATA; Schema: peers; Owner: -
--

COPY peers.certificates (certificate_thumbprint, peer_id, certificate) FROM stdin;
M-lXHT1DRfNebmBxrr9HYYIAx02sn9hynnJBkIGsxn4	01716384713205186433	\\x3082064a30820432a003020102021445fa67b3e0fc3f1b045cb1a4c282c2bbeccfabd0300d06092a864886f70d01010d0500305f310b3009060355040613024e4c311630140603550408130d4e6f6f72642d486f6c6c616e643112301006035504071309416d7374657264616d31163014060355040a130d436f6d6d6f6e2047726f756e64310c300a060355040b13034e4c58301e170d3234303532323133323730305a170d3237303532323133323730305a30818f310b3009060355040613024e4c3110300e06035504081307557472656368743110300e0603550407130755747265636874310d300b060355040a13046f726762312e302c060355040313256d616e616765722d6673632d6e6c782d6d616e616765722d65787465726e616c2e6f726762311d301b06035504051314303137313633383437313332303531383634333330820222300d06092a864886f70d01010105000382020f003082020a0282020100f3a59c0f98baffeeec0f19b180dc87c31eef81a07450a8b38f099e6770247239f6c4ebf50b8960b2c60d27988d4e5319f6099bfb065015062a4a2a0076a0f3528b2de9f4c119181ed52eca6b7097eaecf135be2980ca416683b7498e6d9f1f024780c22064f9b8d56c179685b1465a0d744e4064a2948ca4aa9b3befb7c57d522d781f9d793a3dbf7bbc4d0079930b4b72b37c2387c91b240a26b125374cde313fb61bc3ba29030abdd58348f4d5e9ba2c7376c1919b1129c961f4b084465606a762f2ce5bcf291e82b7b43368e56c5f40ea9cdf4929eacefe72550967b1c75584da21dc0e55d3d0e4a8801af78eac1dda1b3c97880d801d4eb17ba52bc258f2807e4fe428f83f4482e1395262fde73b7fdd0d5664e263de640fe649d08efe3a246e31e697c25742782aec38e3a367f3d9a3beba711f2285448458f55a4642d38f5a7cfd310ce948a4120ed6cc398e28bb9cff1401e068cdabc89896e061d3094432c73794b63b01107e138861e2116c4e6be385947e2afdcd0fe1e28e75c4ae138ff9f83b3efe8b2b6537a522042e1a7129464d709bf963160194dc6ed49a78b37184d275dcc1b15245a066d5e2917528e606f6b0176538b376639728952b086f36d801c4bad046026479fbeaafd9ad285cc91a99cc19322f44b72764d58c5d31fdcac4ba25790fefda7437466a4a80896b6460b8f1120cfd768ce053a95bd70203010001a381cc3081c9300e0603551d0f0101ff0404030205a0301d0603551d250416301406082b0601050507030106082b06010505070302300c0603551d130101ff04023000301d0603551d0e04160414549140b0cb319f3fedefc8e5c537ba9bb3091504301f0603551d230418301680145ae041511187acda6887c000c3065ce2792d89fd304a0603551d110443304182256d616e616765722d6673632d6e6c782d6d616e616765722d65787465726e616c2e6f7267628218696e7761792d6673632d6e6c782d696e7761792e6f726762300d06092a864886f70d01010d0500038202010016e8c037511a101bcc2e52d5969621e2493098234753a9cf8e665898fa8ee155e3699c51baa43f176a8f2563701015c838d04bb32af7fbfbb14d1f5292209d8f0c4aed60bf686592439542189c815a8736cce2d8968a26689a22b5a51d64bbd8014b7b993911c756755714b4431c7ec0a251d47fcfbdd1e6d3909d7a6c7a561cb98ec59002179c42e69728b5475fde91e014b08649f05e4bc61bdc092b3090c512117ca45ea563a73f0d368fedf0e1f69c6c7150cf940526b54a227df9ea76443fe6946fc9138364978497be88ded462f66e8ac01ac4b00b6c0a2a24ea182540fdb4099c9381c37699aa0b3a2a2097ac8488085ebf3a855e63f4fd96f91b5183426759af233cd4fe5d20fb0dd7c4e1c162ad45cb6f199f964423c4bc4110ffbc246c95453dc18d77f455d62df3a2ebe8463d207464e87c2c2d04b8c9dbb12517f585d04eb691b2ba5f82ef16f83c533f23a80a29faa3717bfc3fc6b3efc62f9088beea66bcd6c8df0cdc53a2475b12606372474815d1fef5eaff57348a149fb0313b7a7f690f4de45e8aaa70125e62f5b8678a06a113d64e001d162804b3f73945f1cfb9befe3dcee56c668d7cbeff1ee9b89bf074f72a46ca40e57eba35d5630f4b7521496b7f68e942e09937357e36742d4add7abfc1b89e9cdf788f95bda7a5c610b0bd10fedbe0a6cc0634e8a98f26cad8c76d3b98835f33b5132cb7f670
7FjZxQyckHjVkuNYQ9lrYP6qoGRi-0a_JUjjv-6HetY	01716384655448789995	\\x3082064a30820432a00302010202145325da2a18ae2596624d3d9c5e7ea406a250dbd3300d06092a864886f70d01010d0500305f310b3009060355040613024e4c311630140603550408130d4e6f6f72642d486f6c6c616e643112301006035504071309416d7374657264616d31163014060355040a130d436f6d6d6f6e2047726f756e64310c300a060355040b13034e4c58301e170d3234303532323133323630305a170d3237303532323133323630305a30818f310b3009060355040613024e4c3110300e06035504081307557472656368743110300e0603550407130755747265636874310d300b060355040a13046f726761312e302c060355040313256d616e616765722d6673632d6e6c782d6d616e616765722d65787465726e616c2e6f726761311d301b06035504051314303137313633383436353534343837383939393530820222300d06092a864886f70d01010105000382020f003082020a0282020100b8b80ce63d5d8d3792dbb13e9b02798ab9210640d3cb8e8dcee0c64fb2b8c6961bd1ade6bb179934e04c298eca5dce261445b4127c6787b89646a0ee2b587c2f873c73b47245185bbc61edd2157d5163da7029a32526b6adea000599eecc714f0fdc79a1d9bc1bb0f1a1cbc93f6bbc7385c879f38f9ad8e4f63ca8fd8d12a349354a3ace406990c36c120fa0ac4e5db29b2ca1f13d59d9bb5179157b54f7c43ab5db528430e79fcca18fdba9491407c28fa8a128756d3780bb63fc9596ba423fdc7303b4e2c209d4db899d5aff51edecbf417cbe4aa69e55d3f538649ee0590a37f03226962093cba40116246ce5e4ce5317b54a0db363d6e62e6368334cc8dfcff95f5a6dd3d414284969cf0f13e8e48af6c2e07c19610231438677f8512c984c4ab3c79429a095555bafd3dbf65f682148e62c923840bf422c453f69315bf64794ad057bea14f7d86006756d4dcce18c97a0be343df7e9b9d83ab2ce166b37ab7c3391756909e63a9cbf84d87bf53907724234a12d6ce1841a6147552c0ff1d38e3e93565968203ce2cef599190a0cd9700623512fe18c8a6619bdf2fb311e14be34573ce057c4b59dc84b19fa55196e8cd9fbb6d8130202023efb6aaa0c12834bf797096ddda803f9c174d6301fae9bb1d2aae6f8e085df78f56c7afe9ca3c614ea17d5431aa0510cb6de46b8d3da9e1ed5b929a737f8e7f2e1850680d6ad0203010001a381cc3081c9300e0603551d0f0101ff0404030205a0301d0603551d250416301406082b0601050507030106082b06010505070302300c0603551d130101ff04023000301d0603551d0e0416041413f2aa115c7d190b0b9ca3d0321560aa51e60768301f0603551d230418301680145ae041511187acda6887c000c3065ce2792d89fd304a0603551d110443304182256d616e616765722d6673632d6e6c782d6d616e616765722d65787465726e616c2e6f7267618218696e7761792d6673632d6e6c782d696e7761792e6f726761300d06092a864886f70d01010d05000382020100b79a501c1c599625dd978c3a1c1fd6fd504f11562924b4bb5f7d3b42eca30f80846fe22cb305402d0c0bdfb5a7338cca031bc4d130935ede5d7f3df316776d63af4dbef334103c4b3df3a1680aaf92b5137afc323ee553a274a2c54bb44fb7ac89b72caf518e7134337ca29a02d6b488a01524fa332bb4a5ccb7b83b081e712c58ef788d1f6224dce0a1667d710fc33a4601216dc3bcf3ad90c838a92b290f8065b45a953f4756c5aaa792aabb0da227f0b704feb2dec9f3b15797c65372c3e69572faeac3fa3e7a235f9431e6ed6491212640c84d04e3ed91fc6ba9da78e43301299a490c2d11406eea72200e813abddd658f78ba9fe6e389f11b2d079dbbf79d77c5bad118e3d2962adfd54983b9365f31e8e68dfce5b9a8a1db24097cfb89047b8141a41ce621a88c6a91520c89a0f4e91f09c27d2868271be01e395ce46ae4a117d18bdac5f6e4472c47e1f0a160d433a099a41c03c4751fa7f62cb136b56c2250c95a6c5f31d8b2ed5836b6c6dd686c14959c6a767c35aa835e787bd5ad592863bff683a46516dae0c336082b188fe66f060c3ed4fdf80c8fdcb06207d7bd2b21fc97e90f53d07ebaa1bc19ee2da3bf8736fa3db6b51d5cf756754905fd0bf785d75be2cfaa6104a1ff48c11a507405de513159037fc7b96f5d81426eac768f4a476472350ef7b5f82b9cd128cb151b4e31700627820da5c6174f9f366f
\.


--
-- Data for Name: peers; Type: TABLE DATA; Schema: peers; Owner: -
--

COPY peers.peers (id, name, manager_address) FROM stdin;
01716384713205186433	orgb	https://manager-fsc-nlx-manager-external.orgb:8443
12345678901234567899	fds	https://manager-fsc-nlx-manager-external.fds:8443
01716384655448789995	orga	https://manager-fsc-nlx-manager-external.orga:8443
\.


--
-- Data for Name: schema_migrations; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public.schema_migrations (version, dirty) FROM stdin;
11	f
\.


--
-- Name: content content_pk; Type: CONSTRAINT; Schema: contracts; Owner: -
--

ALTER TABLE ONLY contracts.content
    ADD CONSTRAINT content_pk PRIMARY KEY (hash);


--
-- Name: contract_signatures contract_signatures_pk; Type: CONSTRAINT; Schema: contracts; Owner: -
--

ALTER TABLE ONLY contracts.contract_signatures
    ADD CONSTRAINT contract_signatures_pk PRIMARY KEY (content_hash, signature_type, peer_id);


--
-- Name: failed_distributions distribution_pk; Type: CONSTRAINT; Schema: contracts; Owner: -
--

ALTER TABLE ONLY contracts.failed_distributions
    ADD CONSTRAINT distribution_pk PRIMARY KEY (peer_id, content_hash, action);


--
-- Name: grants_delegated_service_connection grants_delegated_service_connection_pk; Type: CONSTRAINT; Schema: contracts; Owner: -
--

ALTER TABLE ONLY contracts.grants_delegated_service_connection
    ADD CONSTRAINT grants_delegated_service_connection_pk PRIMARY KEY (hash);


--
-- Name: grants_delegated_service_publication grants_delegated_service_publication_pk; Type: CONSTRAINT; Schema: contracts; Owner: -
--

ALTER TABLE ONLY contracts.grants_delegated_service_publication
    ADD CONSTRAINT grants_delegated_service_publication_pk PRIMARY KEY (hash);


--
-- Name: grants_service_connection grants_service_connection_pk; Type: CONSTRAINT; Schema: contracts; Owner: -
--

ALTER TABLE ONLY contracts.grants_service_connection
    ADD CONSTRAINT grants_service_connection_pk PRIMARY KEY (hash);


--
-- Name: grants_service_publication grants_service_publication_pk; Type: CONSTRAINT; Schema: contracts; Owner: -
--

ALTER TABLE ONLY contracts.grants_service_publication
    ADD CONSTRAINT grants_service_publication_pk PRIMARY KEY (hash);


--
-- Name: certificates certificates_pk; Type: CONSTRAINT; Schema: peers; Owner: -
--

ALTER TABLE ONLY peers.certificates
    ADD CONSTRAINT certificates_pk PRIMARY KEY (certificate_thumbprint);


--
-- Name: peers peers_pk; Type: CONSTRAINT; Schema: peers; Owner: -
--

ALTER TABLE ONLY peers.peers
    ADD CONSTRAINT peers_pk PRIMARY KEY (id);


--
-- Name: schema_migrations schema_migrations_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.schema_migrations
    ADD CONSTRAINT schema_migrations_pkey PRIMARY KEY (version);


--
-- Name: grants_delegated_service_connection_content_hash_idx; Type: INDEX; Schema: contracts; Owner: -
--

CREATE INDEX grants_delegated_service_connection_content_hash_idx ON contracts.grants_delegated_service_connection USING btree (content_hash);


--
-- Name: grants_delegated_service_connection_delegator_peer_id_idx; Type: INDEX; Schema: contracts; Owner: -
--

CREATE INDEX grants_delegated_service_connection_delegator_peer_id_idx ON contracts.grants_delegated_service_connection USING btree (delegator_peer_id);


--
-- Name: grants_delegated_service_connection_outway_peer_id_idx; Type: INDEX; Schema: contracts; Owner: -
--

CREATE INDEX grants_delegated_service_connection_outway_peer_id_idx ON contracts.grants_delegated_service_connection USING btree (outway_peer_id);


--
-- Name: grants_delegated_service_connection_service_peer_id_idx; Type: INDEX; Schema: contracts; Owner: -
--

CREATE INDEX grants_delegated_service_connection_service_peer_id_idx ON contracts.grants_delegated_service_connection USING btree (service_peer_id);


--
-- Name: grants_service_connection_consumer_peer_id_idx; Type: INDEX; Schema: contracts; Owner: -
--

CREATE INDEX grants_service_connection_consumer_peer_id_idx ON contracts.grants_service_connection USING btree (consumer_peer_id);


--
-- Name: grants_service_connection_content_hash_idx; Type: INDEX; Schema: contracts; Owner: -
--

CREATE INDEX grants_service_connection_content_hash_idx ON contracts.grants_service_connection USING btree (content_hash);


--
-- Name: grants_service_connection_service_peer_id_idx; Type: INDEX; Schema: contracts; Owner: -
--

CREATE INDEX grants_service_connection_service_peer_id_idx ON contracts.grants_service_connection USING btree (service_peer_id);


--
-- Name: grants_service_publication_directory_peer_id_idx; Type: INDEX; Schema: contracts; Owner: -
--

CREATE INDEX grants_service_publication_directory_peer_id_idx ON contracts.grants_service_publication USING btree (directory_peer_id);


--
-- Name: grants_service_publication_service_peer_id_idx; Type: INDEX; Schema: contracts; Owner: -
--

CREATE INDEX grants_service_publication_service_peer_id_idx ON contracts.grants_service_publication USING btree (service_peer_id);


--
-- Name: certificates_certificate_thumbprint_idx; Type: INDEX; Schema: peers; Owner: -
--

CREATE INDEX certificates_certificate_thumbprint_idx ON peers.certificates USING btree (certificate_thumbprint);


--
-- Name: certificates_peer_id_idx; Type: INDEX; Schema: peers; Owner: -
--

CREATE INDEX certificates_peer_id_idx ON peers.certificates USING btree (peer_id);


--
-- Name: contract_signatures contract_signatures_content_hash_fk; Type: FK CONSTRAINT; Schema: contracts; Owner: -
--

ALTER TABLE ONLY contracts.contract_signatures
    ADD CONSTRAINT contract_signatures_content_hash_fk FOREIGN KEY (content_hash) REFERENCES contracts.content(hash) MATCH FULL ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: contract_signatures contract_signatures_peer_id_fk; Type: FK CONSTRAINT; Schema: contracts; Owner: -
--

ALTER TABLE ONLY contracts.contract_signatures
    ADD CONSTRAINT contract_signatures_peer_id_fk FOREIGN KEY (peer_id) REFERENCES peers.peers(id) MATCH FULL ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: failed_distributions distribution_content_hash; Type: FK CONSTRAINT; Schema: contracts; Owner: -
--

ALTER TABLE ONLY contracts.failed_distributions
    ADD CONSTRAINT distribution_content_hash FOREIGN KEY (content_hash) REFERENCES contracts.content(hash) MATCH FULL ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: failed_distributions distribution_peer_id; Type: FK CONSTRAINT; Schema: contracts; Owner: -
--

ALTER TABLE ONLY contracts.failed_distributions
    ADD CONSTRAINT distribution_peer_id FOREIGN KEY (peer_id) REFERENCES peers.peers(id) MATCH FULL ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: grants_delegated_service_connection grants_delegated_service_connection_content_hash_fk; Type: FK CONSTRAINT; Schema: contracts; Owner: -
--

ALTER TABLE ONLY contracts.grants_delegated_service_connection
    ADD CONSTRAINT grants_delegated_service_connection_content_hash_fk FOREIGN KEY (content_hash) REFERENCES contracts.content(hash) MATCH FULL ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: grants_delegated_service_connection grants_delegated_service_connection_delegator_peer_id_fk; Type: FK CONSTRAINT; Schema: contracts; Owner: -
--

ALTER TABLE ONLY contracts.grants_delegated_service_connection
    ADD CONSTRAINT grants_delegated_service_connection_delegator_peer_id_fk FOREIGN KEY (delegator_peer_id) REFERENCES peers.peers(id) MATCH FULL ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: grants_delegated_service_connection grants_delegated_service_connection_outway_peer_id_fk; Type: FK CONSTRAINT; Schema: contracts; Owner: -
--

ALTER TABLE ONLY contracts.grants_delegated_service_connection
    ADD CONSTRAINT grants_delegated_service_connection_outway_peer_id_fk FOREIGN KEY (outway_peer_id) REFERENCES peers.peers(id) MATCH FULL ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: grants_delegated_service_connection grants_delegated_service_connection_service_peer_id_fk; Type: FK CONSTRAINT; Schema: contracts; Owner: -
--

ALTER TABLE ONLY contracts.grants_delegated_service_connection
    ADD CONSTRAINT grants_delegated_service_connection_service_peer_id_fk FOREIGN KEY (service_peer_id) REFERENCES peers.peers(id) MATCH FULL ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: grants_service_connection grants_service_connection_content_hash_fk; Type: FK CONSTRAINT; Schema: contracts; Owner: -
--

ALTER TABLE ONLY contracts.grants_service_connection
    ADD CONSTRAINT grants_service_connection_content_hash_fk FOREIGN KEY (content_hash) REFERENCES contracts.content(hash) MATCH FULL ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: grants_service_connection grants_service_connections_consumer_peer_id_fk; Type: FK CONSTRAINT; Schema: contracts; Owner: -
--

ALTER TABLE ONLY contracts.grants_service_connection
    ADD CONSTRAINT grants_service_connections_consumer_peer_id_fk FOREIGN KEY (consumer_peer_id) REFERENCES peers.peers(id) MATCH FULL ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: grants_service_connection grants_service_connections_service_peer_id_fk; Type: FK CONSTRAINT; Schema: contracts; Owner: -
--

ALTER TABLE ONLY contracts.grants_service_connection
    ADD CONSTRAINT grants_service_connections_service_peer_id_fk FOREIGN KEY (service_peer_id) REFERENCES peers.peers(id) MATCH FULL ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: grants_service_publication grants_service_publication_directory_peer_id_fk; Type: FK CONSTRAINT; Schema: contracts; Owner: -
--

ALTER TABLE ONLY contracts.grants_service_publication
    ADD CONSTRAINT grants_service_publication_directory_peer_id_fk FOREIGN KEY (directory_peer_id) REFERENCES peers.peers(id) MATCH FULL ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: grants_service_publication grants_service_publication_service_peer_id_fk; Type: FK CONSTRAINT; Schema: contracts; Owner: -
--

ALTER TABLE ONLY contracts.grants_service_publication
    ADD CONSTRAINT grants_service_publication_service_peer_id_fk FOREIGN KEY (service_peer_id) REFERENCES peers.peers(id) MATCH FULL ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: certificates certificates_peer_id_fk; Type: FK CONSTRAINT; Schema: peers; Owner: -
--

ALTER TABLE ONLY peers.certificates
    ADD CONSTRAINT certificates_peer_id_fk FOREIGN KEY (peer_id) REFERENCES peers.peers(id) MATCH FULL ON DELETE CASCADE;


--
-- PostgreSQL database dump complete
--


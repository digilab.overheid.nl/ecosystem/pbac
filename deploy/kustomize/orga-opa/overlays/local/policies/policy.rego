package httpapi.policy

import rego.v1

default allow := {
	"allowed": false,
	"status": {"reason": "permission denied"},
}

allow := {"allowed": true, "status": {"reason": reason}} if {
	check_user_has_valid_vog
	reason := "all clear"
}

# Check if the user does have a valid VOG
check_user_has_valid_vog if {
    user_value := input.headers["User"][0]
    data.employees[user_value].has_vog
}

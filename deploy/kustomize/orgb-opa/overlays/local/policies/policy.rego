package httpapi.policy

import rego.v1

default allow := {
	"allowed": false,
	"status": {"reason": "permission denied"},
}

allow := {"allowed": true, "status": {"reason": reason}} if {
    check_oin_header_is_set
    check_oin_header_is_valid
    check_requested_path_is_not_confidential
	reason := "all clear"
}

# Check if the OIN header is set
check_oin_header_is_set if {
    input.headers["Oin"][0]
}

# Check if the OIN header value is valid
check_oin_header_is_valid if {
    input.headers["Oin"][0] in data.allowed_oins
}

# Check if the requested data is not confidential by path
check_requested_path_is_not_confidential if {
    not startswith(input.path, "/confidential")
}

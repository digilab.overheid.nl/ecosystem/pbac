#!/usr/bin/env bash

set -euo pipefail

kubectl -n fds exec -it -c postgres common-db-1 -- /usr/bin/pg_dump -xO fsc_manager > deploy/fixtures/fds_fsc_manager.sql
kubectl -n fds exec -it -c postgres common-db-1 -- /usr/bin/pg_dump -xO fsc_controller > deploy/fixtures/fds_fsc_controller.sql
kubectl -n orga exec -it -c postgres common-db-1 -- /usr/bin/pg_dump -xO fsc_manager > deploy/fixtures/orga_fsc_manager.sql
kubectl -n orga exec -it -c postgres common-db-1 -- /usr/bin/pg_dump -xO fsc_controller > deploy/fixtures/orga_fsc_controller.sql
kubectl -n orgb exec -it -c postgres common-db-1 -- /usr/bin/pg_dump -xO fsc_manager > deploy/fixtures/orgb_fsc_manager.sql
kubectl -n orgb exec -it -c postgres common-db-1 -- /usr/bin/pg_dump -xO fsc_controller > deploy/fixtures/orgb_fsc_controller.sql

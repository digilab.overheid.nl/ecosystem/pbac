package main

import (
	"encoding/json"
	"fmt"
	"os"

	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/cors"
)

type EmployeeAttributes struct {
	HasVOG bool `json:"has_vog"`
}

type Data struct {
	Employees map[string]EmployeeAttributes `json:"employees"`
}

func main() {
	// Initialize a Fiber server
	app := fiber.New()

	// Allow CORS. IMPROVE: limit to frontend domain
	app.Use(cors.New())

	app.Get("/", func(c *fiber.Ctx) error {
		return c.SendString("PIP editor API")
	})

	app.Get("/employees", func(c *fiber.Ctx) error {
		// Get the data from the JSON file
		content, err := os.ReadFile("employees.json")
		if err != nil {
			return fmt.Errorf("error reading employees: %w", err)
		}

		// Parse the content to make sure the structure is correct
		data := new(Data)
		if err = json.Unmarshal(content, data); err != nil {
			return fmt.Errorf("error parsing employees: %w", err)
		}

		// Return the content
		return c.JSON(data.Employees)
	})

	app.Put("/employees", func(c *fiber.Ctx) error {
		// Parse the request body to make sure its structure is correct
		var data Data
		if err := c.BodyParser(&data.Employees); err != nil {
			return fmt.Errorf("error parsing request body: %w", err)
		}

		// Marshal the content to JSON
		content, err := json.MarshalIndent(data, "", "  ")
		if err != nil {
			return fmt.Errorf("error converting employees: %w", err)
		}

		// Store the data in the JSON file
		if err = os.WriteFile("employees.json", content, 0o644); err != nil {
			return fmt.Errorf("error writing employees: %w", err)
		}

		return c.SendString("success")
	})

	// TODO: add endpoints (and frontend page) for OINs?

	// Start the server on the specified port
	app.Listen(":8080")
}

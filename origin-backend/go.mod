module pip-editor-backend

go 1.22.1

require (
	github.com/go-chi/chi/v5 v5.1.0
	github.com/jessevdk/go-flags v1.6.1
)

require (
	github.com/go-chi/cors v1.2.1
	golang.org/x/sys v0.21.0 // indirect
)

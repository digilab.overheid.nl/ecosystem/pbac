package main

import (
	"io"
	"log"
	"net/http"
)

type handler struct {
	client *http.Client
	domain Domain
}

func New(domain Domain) *handler {
	return &handler{
		client: http.DefaultClient,
		domain: domain,
	}
}

func (h *handler) echo(w http.ResponseWriter, r *http.Request) {
	req, err := http.NewRequestWithContext(r.Context(), http.MethodGet, h.domain.endpoint+"?q=search", nil)
	if err != nil {
		log.Fatal(err)
	}

	req.Header.Add("Fsc-Grant-Hash", h.domain.fscGrantHash)

	headers := []string{"User", "Oin"}
	for _, header := range headers {
		req.Header.Add(header, r.Header.Get(header))
	}

	resp, err := h.client.Do(req)
	if err != nil {
		log.Fatal(err)
	}

	w.WriteHeader(resp.StatusCode)
	if _, err := io.Copy(w, resp.Body); err != nil {
		log.Fatal(err)
	}

	resp.Body.Close()
}

package main

import (
	"log"
	"net/http"

	"github.com/jessevdk/go-flags"

	"github.com/go-chi/chi/v5"
	"github.com/go-chi/chi/v5/middleware"
	"github.com/go-chi/cors"
)

var opts struct {
	AllowedOrigins []string `long:"allowed-origins" description:"Allowed origins for cors" default:"*" env:"APP_ALLOWED_ORIGINS" env-delim:","`
	Domain         struct {
		Endpoint     string `long:"domain-endpoint" description:"Endpoint of the nlx outway" default:"http://outway-fsc-nlx-outway" env:"APP_DOMAIN_ENDPOINT"`
		FscGrantHash string `long:"domain-fsc-grant-hash" description:"FscGrantHash for the endpoint" env:"APP_DOMAIN_FSC_GRANT_HASH" required:"true"`
	}
	ListenAddress string `long:"listen-address" description:"listen address of the app" default:"0.0.0.0:8080" env:"APP_LISTEN_ADDRESS"`
}

type Domain struct {
	endpoint     string
	fscGrantHash string
}

func main() {
	_, err := flags.Parse(&opts)
	if err != nil {
		log.Fatalf("flag parse failed: %v", err)
	}

	// Initialize a Fiber server
	mux := chi.NewRouter()
	mux.Use(middleware.Logger)
	mux.Use(cors.Handler(cors.Options{
		// AllowedOrigins:   []string{"https://foo.com"}, // Use this to allow specific origin hosts
		AllowedOrigins: opts.AllowedOrigins,
		// AllowOriginFunc:  func(r *http.Request, origin string) bool { return true },
		AllowedMethods:   []string{"GET", "POST", "PUT", "DELETE", "OPTIONS"},
		AllowedHeaders:   []string{"*"},
		AllowCredentials: false,
		MaxAge:           300, // Maximum value not ignored by any of major browsers
	}))

	handler := New(Domain{
		endpoint:     opts.Domain.Endpoint,
		fscGrantHash: opts.Domain.FscGrantHash,
	})

	mux.Get("/echo", handler.echo)

	// Start the server on the specified port
	server := http.Server{
		Addr:    opts.ListenAddress,
		Handler: mux,
	}

	log.Printf("Starting server on: %v\n", server.Addr)

	server.ListenAndServe()
}

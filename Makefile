PROJECT_NAME := pbac
CLUSTER_EXISTS := $$(k3d cluster list $(PROJECT_NAME) --no-headers | wc -l | xargs)

.PHONY: k3d
k3d:
	@if [ $(CLUSTER_EXISTS) -eq 0 ]; then \
		KUBECONFIG= k3d cluster create --config=deploy/k3d/config.yaml; \
	else \
		k3d cluster start "$(PROJECT_NAME)"; \
	fi

.PHONY: dev
dev: k3d
	kubectl apply -k deploy/kustomize/fds-fsc/overlays/local
	kubectl apply -k deploy/kustomize/orga-fsc/overlays/local
	kubectl apply -k deploy/kustomize/orgb-fsc/overlays/local
	kubectl apply -k deploy/kustomize/orga-opa/overlays/local
	kubectl apply -k deploy/kustomize/orgb-opa/overlays/local
	kubectl apply -k deploy/kustomize/orgb-http-echo/overlays/local
	skaffold dev --cleanup=false

.PHONY: clean
clean:
	k3d cluster delete "$(PROJECT_NAME)"

.PHONY: reset_fsc_dbs
reset_fsc_dbs:
	@$(MAKE) reset_db NS="fds" ORG=fds DB=fsc_controller DPL=controller-fsc-nlx-controller
	@$(MAKE) reset_db NS="fds" ORG=fds DB=fsc_manager DPL=manager-fsc-nlx-manager
	@$(MAKE) reset_db NS="orga" ORG=orga DB=fsc_controller DPL=controller-fsc-nlx-controller
	@$(MAKE) reset_db NS="orga" ORG=orga DB=fsc_manager DPL=manager-fsc-nlx-manager
	@$(MAKE) reset_db NS="orgb" ORG=orgb DB=fsc_controller DPL=controller-fsc-nlx-controller
	@$(MAKE) reset_db NS="orgb" ORG=orgb DB=fsc_manager DPL=manager-fsc-nlx-manager

.PHONY: reset_db
reset_db:
	# Kill all pod so any active db connections are terminated
	kubectl -n $(NS) scale deployment $(DPL) --replicas=0

	# Copy the fixture to the PostgreSQL data directory in the pod
	kubectl -n $(NS) cp -c postgres deploy/fixtures/$(ORG)_$(DB).sql common-db-1:/var/lib/postgresql/data/dump.sql

	# Drop the existing database
	kubectl -n $(NS) exec common-db-1 -c postgres -- sh -c 'psql -c "DROP DATABASE $(DB);"'

	# Create a new database with the common owner
	kubectl -n $(NS) exec common-db-1 -c postgres -- sh -c 'psql -c "CREATE DATABASE $(DB) WITH OWNER common;"'

	# Restore the database from the fixture
	kubectl -n $(NS) exec common-db-1 -c postgres -- sh -c 'PGPASSWORD=common psql -U common -h 127.0.0.1 $(DB) < /var/lib/postgresql/data/dump.sql'

	# Start the pods
	kubectl -n $(NS) scale deployment $(DPL) --replicas=1

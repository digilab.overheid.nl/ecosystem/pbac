export const prerender = true;
export const trailingSlash = 'always'; // Note: to generate route pages as <route>/index.html, see https://github.com/sveltejs/kit/issues/8770

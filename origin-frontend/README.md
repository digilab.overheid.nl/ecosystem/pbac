# Frontend

## Developing

Install the dependencies once with `pnpm install`. Then, start a development server:

```bash
pnpm dev
```

## Building

To create a production version of your app:

```bash
pnpm build
```

You can preview the production build with `pnpm preview`.
